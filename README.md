# GeriMedical-Assignment (CSV DATA CONSUMER)

The goal was to develop a very simple project in order to upload a CSV file and persist its data to database and consume its data through REST-API.
<br/>

<br/>
This project implemented with the following stack:
<ul>
<li>JAVA, as the base language of project</li>
<li>SPRING-BOOT, as the main framework of the project</li>
<li>H2-DATABASE, lightweight database which needs no configuration, we used its ability to persist data on file and prevent loosing data after restart</li>
<li>HIBERNATE, an open source and lightweight ORM(Object Relational Mapping) tool that help us in interacting with database and persisting data</li>
<li>MAVEN, as the build tool and dependency manager for the project</li>
<li>SWAGGER, is used to have an auto-generated API documentation</li>
</ul>

# INSTRUCTION
<br/>
Based on the project requirements and business logic I have implemented 4 APIs which includes: 
<ul>
<li>An API to upload a CSV file</li>
<li>An API to get all data that was persisted to database from a CSV file</li>
<li>An API to retreive an special record with it's unique "code"</li>
<li>An API to delete all data from database</li>
</ul>


- I have enabled `Swagger-ui` documentation for the project and after running it you can access it using the link below : [localhost:8080/swagger-ui.html]()
- There is also an export of API services for using in Postman application, this file exists in the /resoureces folder of project with name of **GeriMedical.Postman_collection.json**

- Also, I have enabled **H2 Database Console**, you can use it in your browser with following URL: [localhost:8080/h2-console]() , when project is running, needed parameters are :
1. Username: sa
2. Password: sa
3. Url: jdbc:h2:mem:Assignment


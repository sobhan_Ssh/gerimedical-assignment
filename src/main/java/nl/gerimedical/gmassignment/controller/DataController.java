package nl.gerimedical.gmassignment.controller;

/**
 * @Author : Sobhan shakeri
 * @Date : 11/24/2022
 */

import nl.gerimedical.gmassignment.dto.DataDto;
import nl.gerimedical.gmassignment.dto.DefaultResponse;
import nl.gerimedical.gmassignment.util.CSVHelper;
import nl.gerimedical.gmassignment.service.IDataService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/csv/data")
public class DataController {

    final
    IDataService dataService;

    public DataController(IDataService dataService) {
        this.dataService = dataService;
    }

    @PostMapping("/upload")
    public ResponseEntity<DefaultResponse> uploadFile(@RequestParam("file") MultipartFile file) {
        String message = "";

        if (CSVHelper.hasCSVFormat(file)) {
            try {
                dataService.save(file);

                message = "Uploaded the file successfully: " + file.getOriginalFilename();
                return ResponseEntity.status(HttpStatus.OK).body(new DefaultResponse(new Date(), message));
            } catch (Exception e) {
                message = "Could not upload the file: " + file.getOriginalFilename() + "!";
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new DefaultResponse(new Date(), message));
            }
        }
        message = "Please upload a csv file!";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new DefaultResponse(new Date(), message));
    }

    @GetMapping("/all")
    public ResponseEntity<List<DataDto>> getAllData() {
        return new ResponseEntity<>(dataService.getAllData(), HttpStatus.OK);
    }

    @GetMapping("/{code}")
    public ResponseEntity<DataDto> getDataByCode(@PathVariable String code) {
        return new ResponseEntity<>(dataService.getByCode(code), HttpStatus.OK);
    }

    @DeleteMapping("/all")
    public ResponseEntity<DataDto> deleteAllData() {
        dataService.deleteAll();
        return new ResponseEntity<>(HttpStatus.OK);
    }
}

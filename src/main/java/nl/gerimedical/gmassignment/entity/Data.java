package nl.gerimedical.gmassignment.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @Author : Sobhan shakeri
 * @Date : 11/24/2022
 */

@Getter
@Setter
@Entity
@Table(name = "DATA")
public class Data {

    public Data() {

    }
    public Data(String source, String codeListCode, String code, String displayValue, String longDescription, String fromDate, String toDate) {
        this.source = source;
        this.codeListCode = codeListCode;
        this.code = code;
        this.displayValue = displayValue;
        this.longDescription = longDescription;
        this.fromDate = fromDate;
        this.toDate = toDate;
    }

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "ID", columnDefinition = "VARCHAR(255)")
    private String id;

    @NotNull
    @Column(name = "SOURCE")
    private String source;

    @NotNull
    @Column(name = "CODE_LIST_CODE")
    private String codeListCode;

    @NotNull
    @Column(name = "CODE", unique = true)
    private String code;

    @NotNull
    @Column(name = "DISPLAY_VALUE")
    private String displayValue;

    @NotNull
    @Column(name = "LONG_DESCRIPTION")
    private String longDescription;

    @Column(name = "FROM_DATE")
    private String fromDate;

    @Column(name = "TO_DATE")
    private String toDate;



}

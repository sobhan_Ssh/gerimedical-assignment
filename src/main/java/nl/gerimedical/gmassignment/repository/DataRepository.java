package nl.gerimedical.gmassignment.repository;

import nl.gerimedical.gmassignment.entity.Data;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @Author : Sobhan shakeri
 * @Date : 11/24/2022
 */

@Repository
public interface DataRepository extends JpaRepository<Data, String> {

    Optional<Data> findByCode(String code);
}

package nl.gerimedical.gmassignment.service.impl;

import nl.gerimedical.gmassignment.dto.DataDto;
import nl.gerimedical.gmassignment.entity.Data;
import nl.gerimedical.gmassignment.exception.NotFoundException;
import nl.gerimedical.gmassignment.util.CSVHelper;
import nl.gerimedical.gmassignment.repository.DataRepository;
import nl.gerimedical.gmassignment.service.IDataService;
import nl.gerimedical.gmassignment.util.Constant;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @Author : Sobhan shakeri
 * @Date : 11/24/2022
 */

@Service
public class DataServiceImpl implements IDataService {

    private final DataRepository repository;

    public DataServiceImpl(DataRepository repository) {
        this.repository = repository;
    }

    @Override
    public void save(MultipartFile file) {
        try {
            List<Data> data = CSVHelper.csvToData(file.getInputStream());
            repository.saveAll(data);
        } catch (IOException e) {
            throw new RuntimeException("fail to store csv data: " + e.getMessage());
        }
    }

    @Override
    public List<DataDto> getAllData() {
        List<Data> dataList = new ArrayList<>();
        List<DataDto> dtos = new ArrayList<>();
        dataList = repository.findAll();
        if (!CollectionUtils.isEmpty(dataList)) {
            dataList.forEach(data -> dtos.add(new DataDto(data.getSource(), data.getCodeListCode(), data.getCode(),
                    data.getDisplayValue(), data.getLongDescription(), data.getFromDate(), data.getToDate())));
        }
        return dtos;
    }

    @Override
    public DataDto getByCode(String code) {
        Optional<Data> var;
        DataDto dataDto = new DataDto();
        var = repository.findByCode(code);
        if (var.isPresent()){
            Data data = var.get();
            dataDto = new DataDto(data.getSource(), data.getCodeListCode(), data.getCode(), data.getDisplayValue(),
                    data.getLongDescription(), data.getFromDate(), data.getToDate());
        } else
            throw new NotFoundException(Constant.DATA_NOT_FOUND_BY_CODE);
        return dataDto;
    }

    @Override
    public void deleteAll() {
        repository.deleteAll();
    }
}

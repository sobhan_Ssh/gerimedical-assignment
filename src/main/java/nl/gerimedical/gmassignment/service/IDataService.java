package nl.gerimedical.gmassignment.service;

import nl.gerimedical.gmassignment.dto.DataDto;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @Author : Sobhan shakeri
 * @Date : 11/24/2022
 */
public interface IDataService {
    void save(MultipartFile file);

    List<DataDto> getAllData();

    DataDto getByCode(String code);

    void deleteAll();

}

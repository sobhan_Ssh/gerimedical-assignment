package nl.gerimedical.gmassignment.exception;


import nl.gerimedical.gmassignment.dto.DefaultResponse;
import nl.gerimedical.gmassignment.util.Constant;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;

@ControllerAdvice
public class ExceptionTranslator extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Exception.class)
    protected ResponseEntity<DefaultResponse> handleUncaughtException(Exception ex, WebRequest request) {
        DefaultResponse response = new DefaultResponse(new Date(), Constant.DEFAULT_UNEXPECTED_ERROR);
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(NotFoundException.class)
    protected ResponseEntity<DefaultResponse> handleUncaughtException(NotFoundException ex, WebRequest request) {
        DefaultResponse response = new DefaultResponse(new Date(), ex.getMessage());
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

}


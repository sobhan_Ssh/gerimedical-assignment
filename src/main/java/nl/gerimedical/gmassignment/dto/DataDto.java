package nl.gerimedical.gmassignment.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @Author : Sobhan shakeri
 * @Date : 11/24/2022
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DataDto {

    private String source;

    private String codeListCode;

    private String code;

    private String displayValue;

    private String longDescription;

    private String fromDate;

    private String toDate;
}
